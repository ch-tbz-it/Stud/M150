# M150 - E-Business-Applikationen anpassen

[> **Modulidentifikation** ](https://www.modulbaukasten.ch/modul/1875c9da-716c-eb11-b0b1-000d3a830b2b)
- 1	Aufbau der Applikation, Transaktionskonzept, Applikationsumgebung und Rahmenbedingungen (Sicherheit, Performance, Verfügbarkeit, Transaktionsvolumen, usw.) erfassen.
- 2	Vorgabe analysieren, clientseitigen, serverseitigen und datenbankseitigen Änderungsbedarf formulieren.
- 3 Auswirkungen der Änderungen auf Sicherheit und Schutzwürdigkeit der Informationen bei allen beteiligten Komponenten wie Client, Webserver, Applikationsserver und Datenbankserver überprüfen und dokumentieren
- 4 Änderungen inklusive Implementierung und Test (funktional und nicht-funktional) gemäss einem vordefinierten Änderungsprozess planen.
- 5 Änderungen realisieren, testen und dokumentieren.


<https://www.tbzwiki.ch/index.php?title=Modul_150>

<hr>

## LB1 (25%, Mündliche Prüfung, ca. 12 min) - Tag 5: 21.9. und Tag 6: 28.9.2021
 - Wie sind e-Business-Applikationen aufgebaut (Architektur, Aufteilung horizontal / vertikal)?
 - Wie laufen die (internen) Transaktionen ab (Produkt einpflegen, Zahlungsablauf, ...)?
 - Was muss man bezüglich Sicherheit, Performance, Verfügbarkeit, Transaktionsvolumen beachten und was sind die technischen Antworten dafür/dagegen?
 - Was muss beachtet werden bei Änderungsbedarf (Release) "von gut besuchten" (oder gar von hoch kritischen) e-Business-Appl.?
 - Wie macht man Änderungen (Syst. Update) bei "kritischen" oder "hoch verfügbaren" e-Business-Appl. bezüglich DB, Frontend, Backend / Server?
- [Fragenkatalog](./docs/M150_LB1_Fragenkatalog.pdf)

## LB2 (25%, Schriftlicher Auftrag, ca. 200 min) - Tag 7: 5.10.2021
 -> Separates Aufgabenpapier

## LB3 (50%, Vertiefungsarbeit als Dokument oder Produkt, 1-3 Personen) 
- Ziel/Anforderung/Beschreibung für das [Vertiefungsthema](./docs/M150_LB3_Vertiefungsthema.pdf).

**Mögliche Themen für Vertiefungsarbeiten**
- [Paymentsysteme in Web-Shops](./Vertiefungsarbeit-Payment-Systeme.md)
